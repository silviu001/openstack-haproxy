# OpenStack Train - HAProxy API entry point

Before using this tool you need access to a functional OpenStack Train deployment, with administrator rights.

## Edit os_services.sh

### Domain name

* _os_domain='domain.name.to'

Customize the domain name used for external OpenStack Unique API access

The external rewritten URLs will be
* https://domain.name.to/api/SERVICE_NAME/ - where SERVICE_NAME ca be: compute, identity, image, network, placement, volume.

For example, if the Computer API has the following internat API address:

* http://10.100.0.254:8774/v2.1

the new external API address would be:

* https://domain.name.to/api/compute/v2.1

(in a similar manner for the rest of OpenStack API addresses).

### SSL customizations
* **_use_selfsigned** - set to _yes_ if you want to use a self signed certificate.
* **_use_certbot** - set to _yes_ if you want to automatically generate a signed certificate using certbot.
* **_certbot_email** - set your email address to be used when certbot is called to generate a signed certificate.

### _lxc container customizations
* **_lxc_name** - this is the name of the container (no need to modify this value)
* **_lxc_image** - this is name/hash of the LXC image to be used (check available images using _lxc image list_)
* **_lxc_bridge** - the name of the bridge used for communication (check the available bridges using _lxc network list_ and select the bridge used by the other OpenStack services)

### API related values
Next, edit the following entries:

* _os_dashboard_api=''
* _os_compute_api=''
* _os_identity_api=''
* _os_image_api=''
* _os_network_api=''
* _os_placement_api=''
* _os_volume_api=''

For each entry you must add, as a value, the proper OpenStack API information, using **endpoint_ip:endpoint_port format**.

For example, _Compute_ service has the following API URL:

* http://10.100.0.254:8774/v2.1

this will translate into:

* _os_compute_api='10.100.0.254:8774'

## Execute create_proxy.sh

This script will create and configure a HAProxy LXC container. This script should be executed on the _controller_ node, where all the other OpenStack services are hosted as LXC containers.

