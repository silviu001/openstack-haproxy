_os_domain='domain.name.to'
_use_selfsigned='no'
_use_certbot='no'
_certbot_email='email@address.to'

_lxc_name='openstack-haproxy-01'
# lxc image list
_lxc_image='ubuntu:18.04'
# lxc network list
_lxc_bridge='br-enp16s0f0'

_os_dashboard_api=''
_os_compute_api=''
_os_identity_api=''
_os_image_api=''
_os_network_api=''
_os_placement_api=''
_os_volume_api=''

#############################
## DO NOT BELLOW THIS LINE ##
#############################

_os_services=( \
    "__os_dashboard_api_address__ ${_os_dashboard_api}" \
    "__os_compute_api_address__ ${_os_compute_api}" \
    "__os_identity_api_address__ ${_os_identity_api}" \
    "__os_image_api_address__ ${_os_image_api}" \
    "__os_network_api_address__ ${_os_network_api}" \
    "__os_placement_api_address__ ${_os_placement_api}" \
    "__os_volume_api_address__ ${_os_volume_api}" \
)
