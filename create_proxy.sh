#!/bin/bash

set -euo pipefail

if [ ! -f "os_services.sh" ];then
    printf "\n [!!] ERROR: os_services.sh is missing!"
    exit 1
else
    source os_services.sh
fi

if [ ! -f os-haproxy.cfg ];then
    printf "\n [!!] ERROR: os-haproxy.cfg template is missing!"
    exit 1
fi

if [ "${_os_dashboard_api}" == "" ] || \
[ "${_os_compute_api}" == "" ] || \
[ "${_os_identity_api}" == "" ] || \
[ "${_os_image_api}" == "" ] || \
[ "${_os_network_api}" == "" ] || \
[ "${_os_placement_api}" == "" ] || \
[ "${_os_network_api}" == "" ];then
    echo " [!!] ERROR: edit os_services.sh and add proper values for the API's endpoints."
    exit 1
fi
for ((i=0; i<${#_os_services[@]}; i++));do
    _ph=$(echo "${_os_services[$i]}" | cut -d" " -f1)
    _ph_val=$(echo "${_os_services[$i]}" | cut -d" " -f2)
    sed -i "s/${_ph}/${_ph_val}/" os-haproxy.cfg
done

if [ "${_use_selfsigned}" == "no" ] && [ "${_use_certbot}" == "no" ];then
    if [ ! -f 'certificate-bundle.pem' ];then
        echo " [!!] ERROR: certificate-bundle.pem must be provided. It should include certificate, full-chain and private key in PEM format."
        exit 1    
    fi
fi

sed "s/'

lxc init ubuntu:18.04 ${_lxc_name}
lxc network attach ${_lxc_bridge} ${_lxc_name} eth0
lxc start ${_lxc_name}
lxc exec ${_lxc_name} -- apt -y update
lxc exec ${_lxc_name} -- apt -y upgrade
lxc exec ${_lxc_name} -- apt -y install haproxy

if [ "${_use_selfsigned}" == "yes" ];then
    lxc exec ${_lxc_name} -- openssl req -x509 -nodes \
            -subj "/C=RO/L=Timisoara/OU=OpenStack HAProxy LXC/CN=${_os_domain}" \
            -newkey rsa:2048 \
            -days 1000 \
            -keyout /tmp/instance--key.pem \
            -out /tmp/instance--cert.pem
    lxc exec ${_lxc_name} -- cat /tmp/instance--cert.pem /tmp/instance--key.pem > /etc/haproxy/certificate-bundle.pem
    lxc exec ${_lxc_name} -- rm -f /tmp/instance--cert.pem /tmp/instance--key.pem
elif [ "${_use_certbot}" == "yes" ];then
    lxc exec ${_lxc_name} -- add-apt-repository -y ppa:certbot/certbot
    lxc exec ${_lxc_name} -- apt -y update 
    lxc exec ${_lxc_name} -- apt -y certbot
    lxc exec ${_lxc_name} -- certbot --standalone --non-interactive --agree-tos -m ${_certbot_email} -d ${_os_domain}
    lxc exec ${_lxc_name} -- cat /etc/letsencrypt/live/${_os_domain}/cert.pem \
                                /etc/letsencrypt/live/${_os_domain}/fullchain.pem \
                                /etc/letsencrypt/live/${_os_domain}/privkey.pem > /etc/haproxy/certificate-bundle.pem
else
    lxc file push certificate-bundle.pem ${_lxc_name}/etc/haproxy/certificate-bundle.pem
fi
lxc file push os-haproxy.cfg ${_lxc_name}/etc/haproxy/haproxy.cfg
lxc exec ${_lxc_name} -- chown root.root /etc/haproxy/haproxy.cfg
lxc exec ${_lxc_name} -- chmod 640 /etc/haproxy/haproxy.cfg
lxc exec ${_lxc_name} -- chown root.root /etc/haproxy/certificate-bundle.pem
lxc exec ${_lxc_name} -- chmod 640 /etc/haproxy/certificate-bundle.pem
lxc exec ${_lxc_name} -- systemctl start haproxy
lxc exec ${_lxc_name} -- systemctl enable haproxy
_public_ip=$(lxc list ${_lxc_name} --format csv | cut -d',' -f3 | cut -d" " -f1)
printf "\n Use (${_public_ip}) for external access to OpenStack A{Is including Horizon dashboard"
